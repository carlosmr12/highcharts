<?

// Set the JSON header

header("Content-type: text/json");

// The x value is the current JavaScript time, wich is the Unix time multiplied by 1000.
$x = time()*1000;

// The y value is a random number between 0 and 100
$y = rand(0,100);

// Create a PHP array and echo it as JSON
$ret = array($x,$y);
echo json_encode($ret);

?>
