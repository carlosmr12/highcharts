<?php

// Set the JSON header

header("Content-type: text/json");

//MYSQL connection
$link = mysql_connect('localhost','root','carlos');
if(!$link){
    die('Could not connect, check the configuration file!<br />'.mysql_error());
}


mysql_select_db('enviro-net');

$query = mysql_query('SELECT datetime,temperature FROM house_station GROUP BY DATE(datetime) LIMIT 500');

$arr = array();

while($result = mysql_fetch_array($query)){

    $arr_aux = array();

    $aux = new DateTime($result['datetime']);
    $aux = $aux->format('Y-m-d');

    $arr_aux = array(strtotime($aux)*1000, floatval($result['temperature']));

    array_push($arr, $arr_aux);
}

mysql_close($link);

echo json_encode($arr);

?>
